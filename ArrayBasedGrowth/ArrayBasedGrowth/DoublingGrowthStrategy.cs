﻿namespace ArrayBasedGrowth
{
    internal class DoublingGrowthStrategy : IGrowthStrategy
    {
        private const int DEFAULT_SIZE = 4;

        public int newSize(int oldSize)
        {
            return oldSize == 0 ? DEFAULT_SIZE : oldSize * 2;
        }
    }
}