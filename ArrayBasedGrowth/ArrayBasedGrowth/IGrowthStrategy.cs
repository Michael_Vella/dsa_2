﻿namespace ArrayBasedGrowth
{
    public interface IGrowthStrategy
    {
        int newSize(int oldSize);
    }
}