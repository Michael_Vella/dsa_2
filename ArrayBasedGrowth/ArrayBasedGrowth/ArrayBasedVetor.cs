﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArrayBasedGrowth
{
    public class ArrayBasedVetor
    {
        private IGrowthStrategy growthStrategy;
        private DoublingGrowthStrategy doublingGrowthStrategy;

        public ArrayBasedVetor() : this(new DoublingGrowthStrategy()) { }


        public ArrayBasedVetor(IGrowthStrategy growthStrategy) {
            this.growthStrategy = growthStrategy;
        }
        
    }
}
