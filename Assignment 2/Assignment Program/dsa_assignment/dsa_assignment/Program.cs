﻿using dsa_assignment.dynperf_hash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dsa_assignment
{
    class Program
    {
        protected static long UNIVERSE_SIZE = 10000;
        protected static long PRIME = 10007;
        static void Main(string[] args)
        {

            testDynamicImpl(true);
            Console.WriteLine("Dynamic Testing Finished.");
            Console.ReadLine();

            testThreadedDynamicImp(true);
            Console.WriteLine("Threaded Dynamic Testing Finished.");
            Console.ReadLine();
        }


        /// <summary>
        /// This tests the threaded implementation of the DynamicHashing 
        /// Parallel.For is used to iterate through the threaded methods as apposed to 
        /// testDynamic, these are virtually idenitical, except Parallel is meant for threaded 
        /// functions. 
        /// </summary>
        /// <param name="increment"></param>
        private static void testThreadedDynamicImp(bool increment)
        {
            var tasks = new List<Task>();
            long[] testSizes = { 1000, 2500, 5000, 7500, 10000 };
            double[] steps = { .05, .1, .15, .2, .25 };
            long numTests = 5;
            long start, end;

            Console.WriteLine("STARTING DYNAMIC SCALE TEST - SEQUENTIAL DATA\n");

            for (long l = 0; l < steps.Count(); l++)
            {
                Console.WriteLine("Dynamic scale test: Decrement = " + steps[l]);
                Console.ReadLine();
                for (long m = 0; m < numTests; m++)
                {
                    Console.WriteLine("========== Test run #" + (m + 1) + " ==========\n");

                    for (long test = 0; test < testSizes.Count(); test++)
                    {
                        ThreadedDPHashConcrete hash = new ThreadedDPHashConcrete(UNIVERSE_SIZE, PRIME, steps[l], increment);

                        Console.WriteLine("Test size: " + testSizes[test]);

                        Console.WriteLine("Inserting elements into hash...");
                        start = DateTime.Now.Millisecond;

                        Parallel.For(0, testSizes[test], (i) =>
                        {
                            hash.Insert(i, null);
                        });
                        
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished inserting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should be in hash...");
                        start = DateTime.Now.Millisecond;


                        Parallel.For(0, testSizes[test], (i) =>
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have");
                            }
                        });
                        
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should be in hash: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should not be in hash...");
                        start = DateTime.Now.Millisecond;


                        Parallel.For(testSizes[test], 2 * testSizes[test], (j) => 
                        {

                            if (hash.Lookup(j))
                            {
                                Console.WriteLine("Found " + j + " but shouldn't have");
                            }
                        });


                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should not be in hash: " + (end - start) + " ms");

                        Console.WriteLine("Deleting half the elements...");
                        start = DateTime.Now.Millisecond;

                        Parallel.For(0, testSizes[test] / 2, (i) =>
                        {
                            hash.Delete(i);
                        });

                        end = DateTime.Now.Millisecond;
                                           
                        Console.WriteLine("Finished deleting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that were just deleted...");
                        start = DateTime.Now.Millisecond;
                        
                        Parallel.For(0, testSizes[test] / 2, (i) => {
                            if (hash.Lookup(i))
                            {
                                Console.WriteLine("Found " + i + " but shouldn't have");
                            }
                        });
                       
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that were deleted: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should exist...");
                        start = DateTime.Now.Millisecond;


                        Parallel.For(testSizes[test], testSizes[test] / 2, (i) =>
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have.");
                            }

                        });

                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying existing elements: " + (end - start) + " ms");

                        Console.WriteLine("Re-Insert previously deleted elements...");
                        start = DateTime.Now.Millisecond;


                        Parallel.For(0, testSizes[test]/2, (i) =>
                        {
                            hash.Insert(i, null);
                        });

                        //tasks.Insert(Task.Run(() =>
                        //{
                        //    Thread.Sleep(100);
                            
                        //}
                        //));

                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished re-inserting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying all elements that should be in the hash...");
                        start = DateTime.Now.Millisecond;

                        Parallel.For(0, testSizes[test], (i) =>
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have");
                            }
                        });

                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should be in the hash: " + (end - start) + " ms");

                        Console.WriteLine("Deleting all the elements...");
                        start = DateTime.Now.Millisecond;


                        Parallel.For(0, testSizes[test], (i) =>
                        {
                            hash.Delete(i);
                        });
                     

                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished deleting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should not be in hash...");
                        start = DateTime.Now.Millisecond;

                        start = 0;
                        Parallel.For(0, testSizes[test], (j) =>
                        {
                            if (hash.Lookup(j))
                            {
                                Console.WriteLine("Found " + j + " but shouldn't have");
                            }
                        });

                        end = DateTime.Now.Millisecond;

                        Console.WriteLine("Finished querying elements that should not be in hash: " + (end - start) + " ms");

                    }
                }
            }       
        }

        public static void testDynamicImpl(bool increment)
        {
            long[] testSizes = { 1000, 2500, 5000, 7500, 10000 };
            double[] steps = { .05, .1, .15, .2, .25 };
            long numTests = 5;
            long start, end;

            Console.WriteLine("STARTING DYNAMIC SCALE TEST - SEQUENTIAL DATA\n");

            for (long l = 0; l < steps.Count(); l++)
            {
                Console.WriteLine("Dynamic scale test: Decrement = " + steps[l]);

                for (long m = 0; m < numTests; m++)
                {
                    Console.WriteLine("========== Test run #" + (m + 1) + " ==========\n");

                    for (long test = 0; test < testSizes.Count(); test++)
                    {
                        DPHashConcrete hash = new DPHashConcrete(UNIVERSE_SIZE, PRIME, steps[l], increment);

                        Console.WriteLine("Test size: " + testSizes[test]);

                        Console.WriteLine("Inserting elements into hash...");
                        start = DateTime.Now.Millisecond;
                        for (long i = 0; i < testSizes[test]; i++)
                        {
                            hash.Insert(i, null);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished inserting elements: " + (end - start) + " ms");
                       
                        Console.WriteLine("Querying elements that should be in hash...");
                        start = DateTime.Now.Millisecond;
                        for (long i = 0; i < testSizes[test]; i++)
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have");
                            }
                        }

                        int aaaa = 0;
                        DynamicBin val = null;
                        for (int i = 0; i < hash.Dphash.Length; i++)
                        {
                            val = hash.Dphash[i];
                          
                            if (val.actualBin != null)
                            {
                                aaaa++;
                            }
                        }

                        Console.WriteLine("Total amount of entries: {0}", aaaa);
                       // Console.WriteLine("Total times dphash[j].bin[y] was not null: {0}", hash.verifyCounter);
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should be in hash: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should not be in hash...");
                        start = DateTime.Now.Millisecond;
                        for (long j = testSizes[test]; j < 2 * testSizes[test]; j++)
                        {
                            if (hash.Lookup(j))
                            {
                                Console.WriteLine("Found " + j + " but shouldn't have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should not be in hash: " + (end - start) + " ms");

                        Console.WriteLine("Deleting half the elements...");
                        start = DateTime.Now.Millisecond;
                        for (long i = 0; i < testSizes[test] / 2; i++)
                        {
                            hash.Delete(i);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished deleting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that were just deleted...");
                        start = DateTime.Now.Millisecond;
                        for (long i = 0; i < testSizes[test] / 2; i++)
                        {
                            if (hash.Lookup(i))
                            {
                                Console.WriteLine("Found " + i + " but shouldn't have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that were deleted: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should exist...");
                        start = DateTime.Now.Millisecond;
                        for (long i = testSizes[test] / 2; i < testSizes[test]; i++)
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have.");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying existing elements: " + (end - start) + " ms");

                        Console.WriteLine("Re-Insert previously deleted elements...");
                        start = DateTime.Now.Millisecond;
                        for (long i = 0; i < testSizes[test] / 2; i++)
                        {
                            hash.Insert(i, null);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished re-inserting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying all elements that should be in the hash...");
                        start = DateTime.Now.Millisecond;
                        for (long i = 0; i < testSizes[test]; i++)
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should be in the hash: " + (end - start) + " ms");

                        Console.WriteLine("Deleting all the elements...");
                        start = DateTime.Now.Millisecond;
                        for (long i = 0; i < testSizes[test]; i++)
                        {
                            hash.Delete(i);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished deleting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should not be in hash...");
                        start = DateTime.Now.Millisecond;
                        for (long j = 0; j < testSizes[test]; j++)
                        {
                            if (hash.Lookup(j))
                            {
                                Console.WriteLine("Found " + j + " but shouldn't have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should not be in hash: " + (end - start) + " ms");
                    }
                }
            }
        }
    }
}
