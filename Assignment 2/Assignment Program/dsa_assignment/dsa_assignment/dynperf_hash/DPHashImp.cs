﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment.dynperf_hash
{
   public abstract class DPHashImp : DPHash
    {
        /// <summary>
        ///  Maximum size of the universe, U, that can be stored in the DPHash
        /// </summary>
        private static long mAX_UNIVERSE_SIZE = 0;

        /// <summary>
        /// Prime number >= MAX_UNIVERSE_SIZE
        /// </summary>
        private static long p = 0;

        /// <summary>
        /// Constant factor in which the top level of the DPHash grows
        /// </summary>
        private static long c = 2;

        /// <summary>
        /// Number of elements accommodated within hash
        /// </summary>
        private static long m = 0;

        /// <summary>
        /// /Number of sets that top level hash partitions S into 
        /// </summary>
        private static long sm = 0;

        /// <summary>
        /// Hash function currently being used by the DPHash
        /// </summary>
        private HashFunction h = null;

        /// <summary>
        /// Number of updates performed on this DPHash
        /// </summary>
        private long count = 0;

        public static long MAX_UNIVERSE_SIZE { get => mAX_UNIVERSE_SIZE; set => mAX_UNIVERSE_SIZE = value; }
        public static long P { get => p; set => p = value; }
        public static long C { get => c; set => c = value; }
        public static long M { get => m; set => m = value; }
        public static long SM { get => sm; set => sm = value; }
        protected HashFunction H { get => h; set => h = value; }
        protected long Count { get => count; set => count = value; }

        /// <summary>
        /// <see cref="DPHash.Insert(long, object)"/>
        /// </summary>
        /// <param name="x"></param>
        /// <param name="data"></param>
        public abstract void Insert(long x, Object data);
        
        /// <summary>
        /// <seealso cref="DPHash.Delete(long)"/>
        /// </summary>
        /// <param name="x"></param>
        public abstract void Delete(long x);

        /// <summary>
        /// <seealso cref="DPHash.Lookup(long)"/>
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public abstract bool Lookup(long x);
    }
}
