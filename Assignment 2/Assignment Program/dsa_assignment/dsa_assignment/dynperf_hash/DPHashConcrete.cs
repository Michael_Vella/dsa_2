﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment.dynperf_hash
{
    public class DPHashConcrete : DPHashImp
    {
        /** Amount that the subtables should be scaled by */
        private static double sCALE = 2;

        /** Amount that the scale factor of each bin is decremented when needed to be resized */
        private static double sTEP = 0.25;

        /** Flag for whether the bin scale is being incremented or decremented */
        private static bool iNCREMENT = false;

        /** Array representation of the DPHash that contains all bins within the hash */
        private DynamicBin[] dphash = null;

        public static double SCALE { get => sCALE; set => sCALE = value; }
        public static double STEP { get => sTEP; set => sTEP = value; }
        public static bool INCREMENT { get => iNCREMENT; set => iNCREMENT = value; }
        public DynamicBin[] Dphash { get => dphash; set => dphash = value; }


        ///<summary>
        /// Default constructor.  Creates an instance of DPHash using the given maximum
        /// universe size and prime number p that is >= |U|.
        /// </summary>
        public DPHashConcrete(long universeSize, long p, double step, bool increment)
        {
            DPHashImp.MAX_UNIVERSE_SIZE = universeSize;
            DPHashImp.P = p;
            DPHashConcrete.STEP = step;
            DPHashConcrete.INCREMENT = increment;
            this.dphash = new DynamicBin[DPHashImp.SM];

            // setup the hash
            this.Rehash(-1);
        }

       
        /// <summary>
        /// Insert the value x into the hash with the given data.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="data"></param>
        public override void Insert(long x, Object data)
        {
            this.Count++;

            if (this.Count > DPHashConcrete.M)
            {
                this.Rehash(x);
            }
            else
            {
                long j = this.H.Hash(x);
                long location = -1;

                if (this.dphash[j].M > 0)
                {
                    location = this.dphash[j].H.Hash(x);
                }

                if ((location != -1) &&
                    (this.dphash[j].actualBin[location] != null) &&
                    (this.dphash[j].actualBin[location].Value== x))
                {

                    if (this.dphash[j].actualBin[location].IsDeleted)
                    {
                        this.dphash[j].actualBin[location].IsDeleted = false;
                    }
                }
                else
                {
                    this.dphash[j].B++;

                    if (this.dphash[j].B <= this.dphash[j].M)
                    {
                        if (this.dphash[j].actualBin[location] == null)
                        {
                            this.dphash[j].actualBin[location] = new Entry(x, data);
                        }
                        else
                        {
                            List<Entry>  l = new List<Entry>();

                            for (long i = 0; i < this.dphash[j].S; i++)
                            {
                                if (this.dphash[j].actualBin[i] != null && !this.dphash[j].actualBin[i].IsDeleted)
                                {
                                    l.Add(this.dphash[j].actualBin[i]);
                                }

                                // clean up references 
                                this.dphash[j].actualBin[i] = null;
                            }

                            l.Add(new Entry(x, data));

                            bool injective = false;

                            while (!injective)
                            {
                                injective = true;
                                this.dphash[j].actualBin = null;
                                this.dphash[j].actualBin = new Entry[this.dphash[j].S];
                                this.dphash[j].H = UniversalHashFunction.generateHashFunction(this.dphash[j].S);

                                for (int i = 0; i < l.Count(); i++)
                                {
                                    Entry e = (Entry)l.ElementAt(i);

                                    long y = this.dphash[j].H.Hash(e.Value);

                                    if (this.dphash[j].actualBin[y] != null)
                                    {
                                        injective = false;
                                        break;
                                    }

                                    this.dphash[j].actualBin[y] = e;
                                }
                            }

                            l.Clear();
                        }
                    }
                    else
                    {
                        // decrement the scale factor if this is not the first time that this actualBin has been scaled
                        if (this.dphash[j].M != 0)
                        {
                            this.dphash[j].update();
                        }

                        this.dphash[j].M = (long)(this.dphash[j].scale * Math.Max(this.dphash[j].M, 1));
                        this.dphash[j].S = 2 * (this.dphash[j].M) * (this.dphash[j].M - 1);

                        if (this.Verify())
                        {
                            List<Entry>  l = new List<Entry>();

                            if (this.dphash[j].actualBin != null)
                            {
                                for (long i = 0; i < this.dphash[j].actualBin.Length; i++)
                                {
                                    if ((this.dphash[j].actualBin[i] != null) && (!this.dphash[j].actualBin[i].IsDeleted))
                                    {
                                        l.Add(this.dphash[j].actualBin[i]);
                                    }

                                    // clean up references 
                                    this.dphash[j].actualBin[i] = null;
                                }
                            }

                            l.Add(new Entry(x, data));

                            bool injective = false;

                            while (!injective)
                            {
                                
                                injective = true;
                                this.dphash[j].actualBin = null;
                                this.dphash[j].actualBin = new Entry[this.dphash[j].S];
                                this.dphash[j].H = UniversalHashFunction.generateHashFunction(this.dphash[j].S);

                                for (int i = 0; i < l.Count(); i++)
                                {
                                    Entry e = (Entry)l.ElementAt(i);
                                    long y = this.dphash[j].H.Hash(e.Value);

                                    if (this.dphash[j].actualBin[y] != null)
                                    {
                                        injective = false;
                                        break;
                                    }

                                    this.dphash[j].actualBin[y] = e;
                                }
                            }

                            l.Clear();
                        }
                        else
                        {

                            this.Rehash(x);
                        }
                    }
                }
            }
        }
        
        /// <summary>
        /// Delete the value if it exists, then rehash
        /// </summary>
        /// <param name="x"></param>
        public override void Delete(long x)
        {
            this.Count++;

            long j = this.H.Hash(x);

            if (this.dphash[j] != null)
            {
                long location = this.dphash[j].H.Hash(x);

                if (this.dphash[j].actualBin[location] != null)
                {
                    this.dphash[j].actualBin[location].IsDeleted = true;
                }
            }

            if (this.Count >= DPHashImp.M)
            {
                this.Rehash(-1);
            }
        }

        /// <summary>
        /// Returns true if the value x is stored in the hash, and false otherwise.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override bool Lookup(long x)
        {
            long j = this.H.Hash(x);

            if ((this.dphash[j] != null) && (this.dphash[j].B > 0))
            {
                long location = this.dphash[j].H.Hash(x);

                if ((this.dphash[j].actualBin[location] != null) &&
                    (this.dphash[j].actualBin[location].Value == x) &&
                    (!this.dphash[j].actualBin[location].IsDeleted))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Rehashes the entire table as a collision occurred, or the table grew too big
        /// </summary>
        /// <param name="x"></param>
        protected void Rehash(long x)
        {
            List<Entry>  l = new List<Entry>();

            // gather all current entries in hash
            for (long i = 0; i < DPHashImp.SM; ++i)
            {
                if (this.dphash[i] != null && this.dphash[i].actualBin != null)
                {
                    for (long j = 0; j < this.dphash[i].actualBin.Length; ++j)
                    {
                        if ((this.dphash[i].actualBin != null) && (this.dphash[i].actualBin[j] != null))
                        {
                            if (!this.dphash[i].actualBin[j].IsDeleted)
                            {
                                l.Add(this.dphash[i].actualBin[j]);
                            }
                            // clean up any references so that garbace collector can do it's job
                            this.dphash[i].actualBin[j] = null;
                        }
                    }

                    this.dphash[i].H = null;
                    this.dphash[i].actualBin = null;
                    this.dphash[i] = null;
                }
            }

            // get rid of old header table since it is gone now...
            this.dphash = null;

            // add value x if it is in U
            if (x != -1)
            {
                l.Add(new Entry(x));
            }

            // set count to be the number of elements in the hash
            this.Count = l.Count();

            // set value of DPHash.M to be max(count, 4) * (1 + DPHash.C) and allocate header table
            DPHashImp.M = (1 + DPHashImp.C) * Math.Max(this.Count, 4);
            DPHashImp.SM = DPHashImp.M * 2;
            this.dphash = new DynamicBin[DPHashImp.SM];

            // Rehash all elements using a new hash function until we meet the necessary conditions
            List<Entry> [] sublists = new List<Entry>[DPHashImp.SM];

            do
            {
                this.H = UniversalHashFunction.generateHashFunction(DPHashImp.SM);

                // clean up the sublists before we start this procedure
                for (long k = 0; k < DPHashImp.SM; ++k)
                {
                    if (sublists[k] != null)
                    {
                        sublists[k].Clear();
                    }
                    else
                    {
                        sublists[k] = new List<Entry>();
                    }
                }

                for (int i = 0; i < l.Count(); ++i)
                {
                    Entry e = (Entry)l.ElementAt(i);
                    long bucket = this.H.Hash(e.Value);

                    sublists[bucket].Add(e);
                }

                for (long j = 0; j < DPHashImp.SM; ++j)
                {
                    if (sublists[j] == null)
                    {
                        // create empty List<Entry>  for those buckets that didn't get any elements
                        sublists[j] = new List<Entry>();
                    }

                    this.dphash[j] = null;
                    this.dphash[j] = new DynamicBin(DPHashConcrete.SCALE, DPHashConcrete.STEP, DPHashConcrete.INCREMENT);
                    this.dphash[j].B = sublists[j].Count();
                    this.dphash[j].M = (long)(this.dphash[j].scale * this.dphash[j].B);
                    this.dphash[j].S = 2 * (this.dphash[j].M) * (this.dphash[j].M - 1);
                    this.dphash[j].H = UniversalHashFunction.generateHashFunction(this.dphash[j].S);
                }
            } while (!this.Verify());

            l.Clear();

            // condition now holds so Insert the elements into the appropriate bins
            for (long j = 0; j < DPHashImp.SM; ++j)
            {
                if ((sublists[j] != null) && (sublists[j].Count() != 0))
                {
                    bool injective = false;

                    while (!injective)
                    {
                        this.dphash[j].actualBin = null;
                        this.dphash[j].actualBin = new Entry[this.dphash[j].S];
                        this.dphash[j].H = UniversalHashFunction.generateHashFunction(this.dphash[j].S);

                        injective = true;

                        for (int i = 0; i < sublists[j].Count(); ++i)
                        {
                            Entry e = (Entry)sublists[j].ElementAt(i);
                            long location = this.dphash[j].H.Hash(e.Value);

                            if (this.dphash[j].actualBin[location] != null)
                            {
                                injective = false;
                                break;
                            }

                            this.dphash[j].actualBin[location] = e;
                        }
                    }
                }

                sublists[j].Clear();
            }
        }


        ///<summary>
        ///Verify that the following condition holds true:
        /// Sum of all dphash[j].s greater than 32 * M^2 / SM + 4M
        /// </summary>
        protected bool Verify()
        {
            long total = 0;
            long condition = (32 * (DPHashImp.M ^ 2)) / DPHashImp.SM + 4 * DPHashImp.M;

            for (long i = 0; i < this.dphash.Length; i++)
            {
                if (this.dphash[i] != null)
                {
                    total += this.dphash[i].S;
                }

                // small optimization:  if we have already broken the condition, there is no need to keep going...
                if (total > condition)
                {
                    return false;
                }
            }

            return (total <= condition);
        }
    }
}