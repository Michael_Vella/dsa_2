﻿using dsa_assignment.dynperf_hash;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace dsa_assignment.dynperf_hash
{

    /// <summary>
    /// Inherits DPHashConcrete for all the methods. 
    /// All the methods are surrounded by a try{}finaly{} along with an appropriate 
    /// ReaderWriterLockSlim instansiation and thread lock.
    /// </summary>
    public class ThreadedDPHashConcrete : DPHashConcrete
    {
        private ReaderWriterLockSlim cacheLock = new ReaderWriterLockSlim();
        
        public ThreadedDPHashConcrete(long universeSize, long p, double step, bool increment) :
            base(universeSize, p, step, increment)
        {
            
        }

        public override bool Lookup(long x)
        {
            cacheLock.EnterReadLock();
            try
            {
                 return base.Lookup(x);
            }
            finally
            {
                cacheLock.ExitReadLock();
            }
        }

        public override void Insert(long key, object obj)
        {
            cacheLock.EnterWriteLock();
            try
            {
                base.Insert(key, obj);
            }
            finally
            {
                cacheLock.ExitWriteLock();
            }
        }

        public override void Delete(long key)
        {
            cacheLock.EnterWriteLock();
            try
            {
               base.Delete(key);
            }
            finally
            {
                cacheLock.ExitWriteLock();
            }
        }

        public enum AddOrUpdateStatus
        {
            Added,
            Updated,
            Unchanged
        };

        ~ThreadedDPHashConcrete()
        {
            if (cacheLock != null) cacheLock.Dispose();
        }
    }
}