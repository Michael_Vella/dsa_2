﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment.dynperf_hash
{
 
    /// <summary>
    /// Bin is the class that represents a single level-2 bin of the DPHash.  Each bin is
    /// indexed into the DPHash according to the result of hashing the value being
    /// inserted, deleted or queried using the hash function of the DPHash.  The result
    /// of the hash function is the location of the bin where the element is to be located.
    ///
    /// Each bin maintains it's own hash function that is used once again hash the
    /// value being inserted, deleted, or queried into the proper location of the bin.
    /// </summary>   
    public class Bin
    {
        /// <summary>
        /// scale factor in which the size of the bin grows when full
        /// </summary>
        private static long sCALE_FACTOR = 2;

        /// <summary>
        ///Hash function currently in use by this bin.  
        /// </summary>
        private HashFunction h = null;

        /// <summary>
        ///  Number of values currently stored within this bin. 
        /// </summary>
        private long b = 0;

        /// <summary>
        /// Number of elements permitted within this bin 
        /// </summary>
        private long m = 0;

        /// <summary>
        ///  Space allocated to this bin 
        /// </summary>
        private long s = 0;

        /// <summary>
        /// /** */
        /// </summary>
        private Entry[] bin = null;

        /// <summary>
        /// Constructor.  Creats an instance of Bin with the given universal family of hash
        /// functions that is used to randomly generate hash functions.
        ///H universal family of hash functions
        /// </summary>
        public Bin()
        {
        }

        public static long SCALE_FACTOR { get => sCALE_FACTOR; set => sCALE_FACTOR = value; }
        public HashFunction H { get => h; set => h = value; }
        public long B { get => b; set => b = value; }
        public long M { get => m; set => m = value; }
        public long S { get => s; set => s = value; }
        public Entry[] actualBin { get => bin; set => bin = value; }
    }
}
