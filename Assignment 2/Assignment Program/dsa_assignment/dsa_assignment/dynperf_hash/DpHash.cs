﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment.dynperf_hash
{
    /// <summary>
    ///  DPHash is the interface that all implementations of the dynamic perfect hashing data
    ///structure must implement.It declares the methods that must be supported by
    ///any dynamic perfect hashing data structure.
    /// </summary>
    public interface DPHash
    {
        ///<summary>
        ///Insert the element with value x into the hash, and store the given data with the entry. 
        /// </summary>
        /// <param name="x"> x value of the element to be stored </param>
        /// <param name="data"> data to be stored with the inserted element </param>
        void Insert(long x, Object data);

        /// <summary>
        /// Delete the element with the value given from the hash, if it exists.
        /// </summary>
        /// <param name="x"> The value to be deleted.</param>
        void Delete(long x);
        
        ///<summary>
        /// Returns true if the element with the value given is found in the hash and false otherwise.
        /// </summary>
        /// <param name="x"> value to be queried</param>
        /// <returns> true if the elemenet with the given value is found, false otherwise.</returns>
        bool Lookup(long x);
    }
}
