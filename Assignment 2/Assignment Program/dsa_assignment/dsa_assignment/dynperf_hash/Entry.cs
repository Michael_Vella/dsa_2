﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment.dynperf_hash
{
    ///<summary>
    ///Entry is the class that represents a single element within the DPHash.  It stores the value
    /// of the entry, as well the data stored with the entry.  Also, the entry has a flag that
    /// specifies whether the entry has been deleted from the DPHash and should be removed 
    /// once a rehashing occurs.
    /// </summary>
    public class Entry
    {
        /** Value of this entry */
        private long value = -1;

        /** Data stored with this entry */
        private Object data = null;

        /** Flag telling whether this entry has been deleted and should be removed or not */
        private bool isDeleted = false;

        public long Value { get => value; set => this.value = value; }
        public object Data { get => data; set => data = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }

        ///<summary>
        /// Constructor.  Creates and instance of Entry with the given value.
        /// </summary>
        public Entry(long value)
        {
            this.value = value;
            this.data = null;
        }

        ///<summary>
        /// Constructor.  Creates an instance of Entry with the given value and data.
        /// </summary>
        public Entry(long value, Object data)
        {
            this.value = value;
            this.data = data;
        }

    }
        
}
