﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment.dynperf_hash
{
    public class HashFunction
    {
        //Variables of the hash function
        protected long a = -1;
        protected long b = -1;
        protected long m = -1;

        
        /// <summary>
        /// Constructor.Creates an instance of HashFunction that is used for hashing a value.
        ///
        /// </summary>
        /// <param name="a"> (DPHash.P-2) >= a >= 1 </param>
        /// <param name="b"> (DPHash.P-2) >= b >= 0 </param>
        /// <param name="m"> size of the table that this hash function sends elements to </param>
        public HashFunction(long a, long b, long m)
        {
            this.a = a;
            this.b = b;
            this.m = m;
        }

        /// <summary>
        /// Hash the given value and return the result.
        /// </summary>
        /// <param name="x"> value to be hashed </param>
        /// <returns> result of hasing the given value using this hash function </returns>
        public long Hash(long x)
        {
            long result = (((a * x + b) % DPHashImp.P) % this.m);

            return result;
        }

        /// <summary>
        /// H_s = { 
        ///         (a * x mod 2^w)/2 ^ w - m
	    ///          as long as 'a' != 0
	    ///       }
        ///
        ///  m = 2^m,
        ///  w = 32 for ints, 64 for long.
        /// </summary>
        /// <param name="x">value to be hashed</param>
        /// <returns></returns>
        public long Hash_2(long x) {

            //long result = (a * x % (int)Math.Pow(2,w))/(int)Math.Pow(2,w-m);

            int _b = 0;
            long result = (a * x + _b) % DPHashImp.P %this.m;

            return result;
        }

        /// <summary>
        /// Decides on a hash function to be used. 
        /// </summary>
        /// <param name="x">the value to be hashed.</param>
        /// <returns></returns>
        public long GenHash(long x) {
            Random random = new Random();
            int chosenHash = random.Next(1, 2);

            if (chosenHash == 1)
            {
                return Hash(x);
            }
            else
            {
                return Hash_2(x);
            }

        }


        public String toString()
        {
            return "a = " + a + ", b = " + b;
        }
    }
}
