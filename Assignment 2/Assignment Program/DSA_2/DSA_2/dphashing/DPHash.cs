﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSA_2.dphashing
{
    public interface DPHash
    {
        /// <summary>
        /// Insert the element with value x into the hash, 
        /// and store the given data withthe entry.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="data"></param>
        void Insert(long x, Object data);

        /// <summary>
        /// Delete the element with the value given from the hash, if it exists.
        /// </summary>
        /// <param name="x"></param>
        void Delete(long x);

        /// <summary>
        /// Returns true if the element with the value given is found in the hash 
        /// and false otherwise.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        bool Lookup(long x);
    }
}
