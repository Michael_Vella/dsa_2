﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSA_2.dphashing
{
    public abstract class DpHash_Imp : DPHash
    {
        /// <summary>
        /// Maxium size of the univerese, U, that can be stored in the DPHash
        /// </summary>
        public static long MAX_UNIVERSE_SIZE = 0;

        /// <summary>
        /// Prime number >= U 
        /// </summary>
        public static long P = 0;

        /// <summary>
        /// Constant factor in which the top level of DP hash grows (ie doubling method)
        /// </summary>
        public static long C = 2;

        /// <summary>
        /// Number of elements accomdidated within the hash table.
        /// </summary>
        public static long M = 0;

        /// <summary>
        /// Number of sets that the top level hash partitions S into. 
        /// </summary>
        public static long SM = 0;

        /// <summary>
        /// Hash function currently being used
        /// </summary>
        protected HashFunction h = null;

        /// <summary>
        /// Number of updates.
        /// </summary>
        protected long count = 0;

        /// <summary>
        /// Delete the element with the value given from the hash, if it exists.
        /// </summary>
        /// <param name="x"></param>
        public abstract void Delete(long x);


        /// <summary>
        /// Insert the element with value x into the hash, 
        /// and store the given data withthe entry.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="data"></param>
        public abstract void Insert(long x, object data);

        /// <summary>
        /// Returns true if the element with the value given is found in the hash 
        /// and false otherwise.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public abstract bool Lookup(long x);
    }
}
