﻿namespace DSA_2.dphashing
{
    public class Bin
    {
        /// <summary>
        /// Factor by which the bin grows.
        /// </summary>
        public static long SCALE_FACTOR = 2;

        /// <summary>
        /// Hash function currently used by the bin.
        /// </summary>
        public HashFunction h = null;

        /// <summary>
        /// Number of values currently stored within the bin.
        /// </summary>
        public long b = 0;

        /// <summary>
        /// Number of elements allowed in bin.
        /// </summary>
        public long m = 0;

        /// <summary>
        /// Allocated space
        /// </summary>
        public long s = 0;

        /// <summary>
        /// Array representation of the bin that store all entries in the bin
        /// </summary>
        public Entry[] bin = null;
        
    }
 }