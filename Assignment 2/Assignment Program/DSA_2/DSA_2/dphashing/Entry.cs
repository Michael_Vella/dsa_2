﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSA_2.dphashing
{
    public class Entry
    {
        /** Value of this entry */
        private long value = -1;
        /** Data stored with this entry */
        private Object data = null;
        /** Flag telling whether this entry has been deleted and should be removed or not */
        private bool isDeleted = false;

        public long Value { get => value; set => this.value = value; }
        public object Data { get => data; set => data = value; }
        public bool IsDeleted { get => isDeleted; set => isDeleted = value; }

        /**
        * Constructor.  Creates and instance of Entry with the given value.
        * 
        * @param value value of the entry being created
        */
        public Entry(long value)
        {
            this.value = value;
            this.data = null;
        }

        public Entry(long value, Object data)
        {
            this.value = value;
            this.data = data;
        }


    }
}
