﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSA_2.dphashing
{
    public class DPHashConcrete : DpHash_Imp
    {

        /// <summary>
        /// The amount that subtables should be scaled by
        /// </summary>
        public static double SCALE = 2;

        /// <summary>
        /// Amount that the scale factor of each bin is decremented when needed to be resized 
        /// </summary>
        public static double STEP = 0.25;
        /// <summary>
        /// Flag for whether the bin scale is being incremented or decremented 
        /// </summary>
        public static bool INCREMENT = false;
        
        /// <summary>
        /// The array representation of the DPhash that contains all the bins within the hash
        /// </summary>
        protected DynamicBin[] binDPHash = null;

        /// <summary>
        /// Creats an instance of the hash  using the given maximum 
        /// universe size, and prime number that is >= |U|
        /// </summary>
        /// <param name="universeSize"> Maximum value that can be inserted</param>
        /// <param name="p"> prime number that is >= universeSize</param>
        /// <param name="scale"> constant factor that DpHash.M is bigger than n </param>
        public DPHashConcrete(int universeSize, long p, double step, bool increment) {
            DpHash_Imp.MAX_UNIVERSE_SIZE = universeSize;
            DpHash_Imp.P = p;
            DPHashConcrete.STEP = step;
            DPHashConcrete.INCREMENT = increment;
            this.binDPHash = new DynamicBin[DpHash_Imp.SM];

            //set up the hash
            this.Rehash(-1);
        }


        public override void Insert(long x, object data)
        {
            this.count++;
            if (this.count > DPHashConcrete.M)
            {
                this.Rehash(x);
            }
            else
            {
                long j = this.h.Hash(x);
                long location = -1;
               while (j < 0 )
                {
                    j = this.h.Hash(x);
                }
                if (this.binDPHash[j].m > 0)
                {
                    location = this.binDPHash[j].h.Hash(x);
                }

                if ((location != -1)
                    && (this.binDPHash[j].bin[location] != null)
                    && (this.binDPHash[j].bin[location].Value == x)
                   )
                {
                    if (this.binDPHash[j].bin[location].IsDeleted) {
                        this.binDPHash[j].bin[location].IsDeleted = false;
                    }

                }
                else
                {
                    this.binDPHash[j].b++;

                    if (this.binDPHash[j].b <= this.binDPHash[j].m)
                    {
                        if (this.binDPHash[j].bin[location] == null)
                        {
                            this.binDPHash[j].bin[location] = new Entry(x, data);
                        }
                        else
                        {
                            List<Entry> l = new List<Entry>();

                            for (long i = 0; i < this.binDPHash[j].s; i++)
                            {
                                if(this.binDPHash[j].bin[i] != null && !this.binDPHash[j].bin[i].IsDeleted)
                                {
                                    l.Add(this.binDPHash[j].bin[i]);
                                }                            
                            }

                            l.Add(new Entry(x, data));

                            bool injective = false;

                            while (!injective)
                            {
                                injective = true;

                                this.binDPHash[j].bin = null;
                                this.binDPHash[j].bin = new Entry[this.binDPHash[j].s];
                                this.binDPHash[j].h = UniversalHashFunction.generaterHashFunction(this.binDPHash[j].s);

                                for (int i = 0; i < l.Count; i++)
                                {
                                    Entry entry = (Entry)l.ElementAt(i);
                                    long y = this.binDPHash[j].h.Hash(entry.Value);

                                    if (this.binDPHash[j].bin[y] != null)
                                    {
                                        injective = false;
                                        break;
                                    }
                                    this.binDPHash[j].bin[y] = entry;
                                }
                            }

                            l.Clear();

                        }
                    }
                    else
                    {
                        //decrement the scale factor if this is not the first time that this bin has been scaled.
                        if (this.binDPHash[j].m != 0)
                        {
                            this.binDPHash[j].update();

                            this.binDPHash[j].m = (long)(this.binDPHash[j].scale * Math.Max(this.binDPHash[j].m, 1));
                            this.binDPHash[j].s = 2 * (this.binDPHash[j].m) * (this.binDPHash[j].m - 1);

                            if (this.Verify())
                            {
                                List<Entry> l = new List<Entry>();

                                if (this.binDPHash[j].bin != null)
                                {
                                    for (long i = 0; i < this.binDPHash[j].bin.Length; i++)
                                    {
                                        if ((this.binDPHash[j].bin[i] != null ) && (!this.binDPHash[j].bin[i].IsDeleted))
                                        {
                                            l.Add(this.binDPHash[j].bin[i]);
                                        }
                                        //clean up references
                                        this.binDPHash[j].bin[i] = null;


                                    }
                                }
                                l.Add(new Entry(x, data));

                                bool injective = false;

                                while (!injective)
                                {
                                    injective = true;
                                    this.binDPHash[j].bin = null;
                                    this.binDPHash[j].bin = new Entry[this.binDPHash[j].s];
                                    this.binDPHash[j].h = UniversalHashFunction.generaterHashFunction(this.binDPHash[j].s);

                                    for (int i = 0; i < l.Count; i++)
                                    {
                                        Entry e = (Entry)l.ElementAt(i);
                                        long y = this.binDPHash[j].h.Hash(e.Value);
                                        if (this.binDPHash[j].bin[y] != null) 
                                        {
                                            injective = false;
                                            break;
                                        }
                                        this.binDPHash[j].bin[y] = e;

                                    }
                                }
                                l.Clear();
                            }
                            else
                            {
                                this.Rehash(x);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Delete the value of x if it exists.
        /// </summary>
        /// <param name="x"></param>
        public override void Delete(long x)
        {
            this.count++;
            long j = this.h.Hash(x);

            if (this.binDPHash[j] != null)
            {
                long location = this.binDPHash[j].h.Hash(x);

                if (this.binDPHash[j].bin[location] != null)
                {
                    this.binDPHash[j].bin[location].IsDeleted = true;
                }
            }

            if (this.count >= DpHash_Imp.M) {
                this.Rehash(-1);
            }
        }

        /// <summary>
        ///  Returns true if the value x is store in the hash, false otherwise.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public override bool Lookup(long x)
        {
            long j = this.h.Hash(x);

            if ((this.binDPHash[j] != null ) &&  (this.binDPHash[j].b > 0))
            {
                long location = this.binDPHash[j].h.Hash(x);

                if ((this.binDPHash[j].bin[location] != null)
                    && (this.binDPHash[j].bin[location].Value == x)
                    && (!this.binDPHash[j].bin[location].IsDeleted))
                {
                    return true;
                }
            }
            return false;

        }

        /// <summary>
        /// Rehashes the entire table when a collision occurs/ when the table grows to big.
        /// </summary>
        /// <param name="x">value that cause the rehash, -1 if the deletion casues the rehash</param>
        private void Rehash(long x)
        {
            List<Entry> l = new List<Entry>();

            //Get all current entries in the hash
            for (long i = 0; i < DpHash_Imp.SM ; ++i)
            {
                if (this.binDPHash[i] != null && this.binDPHash[i].bin != null)
                {
                    for (long j = 0; j < binDPHash[i].bin.Length; ++j)
                    {
                        if (this.binDPHash[i].bin != null && this.binDPHash[i].bin[j] != null)
                        {
                            //IsDeleted is set to false automatically so the values isnt flaged for deletion.
                            if (!this.binDPHash[i].bin[j].IsDeleted)
                            {
                                l.Add(this.binDPHash[i].bin[j]);
                            }
                            //remove the table.
                            this.binDPHash[i].bin[j] = null;
                        }

                    }

                    this.binDPHash[i].h = null;
                    this.binDPHash[i].bin = null;
                    this.binDPHash[i] = null;

                }
            }

            this.binDPHash = null;

            //add the value of x
            if (x != -1)
            {
                l.Add(new Entry(x));
            }

            //set the count to the elements of the hash
            this.count = l.Count;

            //set the value of DPHash_Imp.M to be max(count,4) * (1 + DPHash.C) and allocate table
            DpHash_Imp.M = (1 + DpHash_Imp.C) * Math.Max(this.count, 4);
            DpHash_Imp.SM = DpHash_Imp.M * 2;
            this.binDPHash = new DynamicBin[DpHash_Imp.SM];


            //rehash all the elements using a new hash function until we meet the necessary conditionss
            List<Entry>[] sublists = new List<Entry>[DpHash_Imp.SM];

            do
            {
                this.h = UniversalHashFunction.generaterHashFunction(DpHash_Imp.SM);

                //Clean up sublists before we start the rehash
                for (long k = 0; k < DpHash_Imp.SM; ++k)
                {
                    if (sublists[k] != null)
                    {
                        sublists[k].Clear();
                    }
                    else
                    {
                        sublists[k] = new List<Entry>();
                    }
                }

                for (int i = 0; i < l.Count; ++i)
                {
                    Entry entry = l.ElementAt(i);
                    long bucket = this.h.Hash(entry.Value);

                    sublists[bucket].Add(entry);
                }

                for (long j = 0; j < DpHash_Imp.SM; ++j)
                {
                    if (sublists[j] == null)
                    {
                        // create empty list for those buckets that didn't get any elements
                        sublists[j] = new List<Entry>();
                    }

                    this.binDPHash[j] = null;
                    this.binDPHash[j] = new DynamicBin(DPHashConcrete.SCALE,DPHashConcrete.STEP,DPHashConcrete.INCREMENT);
                    this.binDPHash[j].b = sublists[j].Count;
                    this.binDPHash[j].m = (long) this.binDPHash[j].scale * this.binDPHash[j].b;
                    this.binDPHash[j].s = 2 * (this.binDPHash[j].m) * (this.binDPHash[j].m - 1);
                    this.binDPHash[j].h = UniversalHashFunction.generaterHashFunction(this.binDPHash[j].s);
                }
            } while (!this.Verify());

            l.Clear();

            // Condition now holds so insert the elements into the appropriate bins 
            for (long j = 0; j < DpHash_Imp.SM; ++j)
            {
                if ((sublists[j] != null) && (sublists[j].Count != 0 ))
                {
                    bool injective = false;

                    while (!injective)
                    {
                        this.binDPHash[j].bin = null;
                        this.binDPHash[j].bin = new Entry[this.binDPHash[j].s];
                        this.binDPHash[j].h = UniversalHashFunction.generaterHashFunction(this.binDPHash[j].s);

                        injective = true;

                        for (int i = 0; i < sublists[j].Count; ++i)
                        {
                            Entry entry = (Entry) sublists[j].ElementAt(i);
                            long location = this.binDPHash[j].h.Hash(entry.Value);

                            if (this.binDPHash[j].bin[location] != null)
                            {
                                injective = false;
                                break;

                            }

                            this.binDPHash[j].bin[location] = entry;
                        }
                    }
                }
                sublists[j].Clear();
            }
        }


        /// <summary>
        /// Verify that following condition holds true:
        /// sum of all binDPHash[j].s greater than 32 * m^2 / SM + 4m  
        /// </summary>
        /// <returns>true if the condition is holds, false otherwise</returns>
        private bool Verify()
        {
            long total = 0;
            long condition = (32 * ((long)Math.Pow(DpHash_Imp.M, 2))) / DpHash_Imp.SM + 4 * DpHash_Imp.M;

            for (long i = 0; i < this.binDPHash.Length; i++)
            {
                if (this.binDPHash[i] != null)
                {
                    total += this.binDPHash[i].s;
                }

                //if the condition is already broken no need to continue itereating. 
                if (total > condition)
                {
                    return false;
                }
            }

            return (total <= condition);
        }
    }
}
