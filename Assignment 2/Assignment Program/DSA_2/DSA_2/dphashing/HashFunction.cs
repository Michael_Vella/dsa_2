﻿namespace DSA_2.dphashing
{
    public class HashFunction
    {
        /** Variables of the hash function */
        protected long a = -1;
        protected long b = -1;
        protected long m = -1;


        /// <summary>
        /// Creates an instance of HashFunction that is used for hashing a value.
        /// </summary>
        /// <param name="a"> (DPHash.P-1) >= a >= 1 </param>
        /// <param name="b"> (DPHash.P-1) >= b >= 0 </param>
        /// <param name="m"> size of the table that this hash function sends elements to </param>
        public HashFunction(long a, long b, long m)
        {
            this.a = a;
            this.b = b;
            this.m = m;
        }

        /// <summary>
        ///  Hahs the given value
        /// </summary>
        /// <param name="x"></param>
        /// <returns> result of the hash value</returns>
        public long Hash(long x) {

           
            long result = (((a * x + b) % DpHash_Imp.P) % this.m);

            return result;
        }

        public string ToString()        {
            return "a = " + a + ", b = " + b;
        }
    }
}