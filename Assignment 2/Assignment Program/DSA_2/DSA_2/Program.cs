﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSA_2.dphashing;
namespace DSA_2
{
    class Program
    {

        protected static int UNIVERSE_SIZE = 10000;

        protected static int PRIME = 10007;
        static void Main(string[] args) {
            testDynamicImpl(true);
            Console.ReadLine();
        }

        public static void testDynamicImpl(bool increment)
        {
            int[] testSizes = { 1000, 2500, 5000, 7500, 10000 };
            double[] steps = { .05, .1, .15, .2, .25 };
            int numTests = 5;
            long start, end;

            Console.WriteLine("STARTING DYNAMIC SCALE TEST - SEQUENTIAL DATA\n");

            for (int l = 0; l < steps.Count(); l++)
            {
                Console.WriteLine("Dynamic scale test: Decrement = " + steps[l]);

                for (int m = 0; m < numTests; m++)
                {
                    Console.WriteLine("========== Test run #" + (m + 1) + " ==========\n");

                    for (int test = 0; test < testSizes.Count(); test++)
                    {
                        DPHashConcrete hash = new DPHashConcrete(UNIVERSE_SIZE,PRIME, steps[l], increment);

                        Console.WriteLine("Test size: " + testSizes[test]);

                        Console.WriteLine("Inserting elements into hash...");
                        start = DateTime.Now.Millisecond;
                        for (int i = 0; i < testSizes[test]; i++)
                        {
                            hash.Insert(i, null);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished inserting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should be in hash...");
                        start = DateTime.Now.Millisecond;
                        for (int i = 0; i < testSizes[test]; i++)
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should be in hash: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should not be in hash...");
                        start = DateTime.Now.Millisecond;
                        for (int j = testSizes[test]; j < 2 * testSizes[test]; j++)
                        {
                            if (hash.Lookup(j))
                            {
                                Console.WriteLine("Found " + j + " but shouldn't have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should not be in hash: " + (end - start) + " ms");

                        Console.WriteLine("Deleting half the elements...");
                        start = DateTime.Now.Millisecond;
                        for (int i = 0; i < testSizes[test] / 2; i++)
                        {
                            hash.Delete(i);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished deleting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that were just deleted...");
                        start = DateTime.Now.Millisecond;
                        for (int i = 0; i < testSizes[test] / 2; i++)
                        {
                            if (hash.Lookup(i))
                            {
                                Console.WriteLine("Found " + i + " but shouldn't have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that were deleted: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should exist...");
                        start = DateTime.Now.Millisecond;
                        for (int i = testSizes[test] / 2; i < testSizes[test]; i++)
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have.");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying existing elements: " + (end - start) + " ms");

                        Console.WriteLine("Re-Insert previously deleted elements...");
                        start = DateTime.Now.Millisecond;
                        for (int i = 0; i < testSizes[test] / 2; i++)
                        {
                            hash.Insert(i, null);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished re-inserting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying all elements that should be in the hash...");
                        start = DateTime.Now.Millisecond;
                        for (int i = 0; i < testSizes[test]; i++)
                        {
                            if (!hash.Lookup(i))
                            {
                                Console.WriteLine("Couldn't find " + i + " but should have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should be in the hash: " + (end - start) + " ms");

                        Console.WriteLine("Deleting all the elements...");
                        start = DateTime.Now.Millisecond;
                        for (int i = 0; i < testSizes[test]; i++)
                        {
                            hash.Delete(i);
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished deleting elements: " + (end - start) + " ms");

                        Console.WriteLine("Querying elements that should not be in hash...");
                        start = DateTime.Now.Millisecond;
                        for (int j = 0; j < testSizes[test]; j++)
                        {
                            if (hash.Lookup(j))
                            {
                                Console.WriteLine("Found " + j + " but shouldn't have");
                            }
                        }
                        end = DateTime.Now.Millisecond;
                        Console.WriteLine("Finished querying elements that should not be in hash: " + (end - start) + " ms");
                    }
                }
            }
        }
    }
}
