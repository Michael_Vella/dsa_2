﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruskals_Algo
{
    public class WeightedUndirecteEdge : UndirectedEdge
    {
        public int Wieght { get; private set; }

        public WeightedUndirecteEdge(UndirectedEdge edge, int weight): this(edge.V1,edge,V2,weight) {
           
        }

        public WeightedUndirecteEdge(String V1, String V2, int weight):base(V1,V2) {
            this.Wieght = weight;

        }
    }

}
