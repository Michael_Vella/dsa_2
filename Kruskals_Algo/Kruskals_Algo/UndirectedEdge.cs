﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruskals_Algo
{
    public class UndirectedEdge
    {
        private String v1;
        private String v2;

        public UndirectedEdge(String V1, String V2) {
            this.V1 = V1;
            this.V2 = V2;
        }

        protected string V1 { get => v1; set => v1 = value; }
        protected string V2 { get => v2; set => v2 = value; }

        public override bool Equals(object obj)
        {
            var edge = obj as UndirectedEdge;

            //this instance is NOT NULL, the parameter pass (obj) is NULL
            if (edge == null)
            {
                return false;//therefore not equal
            }

            
            if (V1 == edge.V1 && V2 == edge.V2)
            {
                //this instance is (V1,V2) and the other instance is also (V1,V2)
                return true;
            }

            if (V1 == edge.V2  && V2 == edge.V1)
            {
                //this instance is (V1,V2) and the other instance is also (V2,V1)
                //since this is an undirected edge, the order of the vertices is not important.
                return true;
            }

            return false;//vertices not the same.

            
            //Same as the above
            //return edge != null &&
            //             (V1 == edge.V1 &&
            //              V2 == edge.V2) ||
            //              (V1 == edge.V2 &&
            //              V2 == edge.V1);
        }
        
        public override int GetHashCode()
        {

            ////Option 1 
            var hashCode = 1989511945;
            if (V1.CompareTo(V2) > 0)
            {
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V1);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V2);
            }
            else
            {
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V2);
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V1);
            }
            return hashCode;

            //Option 2 
            //var hashCode1 = 1989511945;
            //hashCode1 = hashCode1 * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V1);
            //hashCode1 = hashCode1 * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V2);

            //var hashCode2 = 1989511945;
            //hashCode2 = hashCode2 * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V2);
            //hashCode2 = hashCode2 * -1521134295 + EqualityComparer<string>.Default.GetHashCode(V1);

            //return hashCode1 + hashCode2;
            
        }

    }
}
