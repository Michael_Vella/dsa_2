﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphSearching
{
    public class GraphAdjancencyList
    {

        private bool isUndirected;

        public GraphAdjancencyList(bool IsUndirected = true)
        {
            this.isUndirected = IsUndirected;
        }

        //Dicitionary is a hashtable that uses generics in c#
        /// <summary>
        /// The Data for the grpah is stored here as an adjacency list 
        /// </summary>        
        private Dictionary<
                String, //keys represent vertices 
                LinkedList<String>// The value (for each key) is a linked list of adjacent vertices!
            > g = new Dictionary<string, LinkedList<string>>();


        public bool Contains(String v)
        {
            return g.ContainsKey(v); // The dictionary provbides this metod with an average time of o(1)
        }

        public void AddVertex(String v)
        {
            g.Add(v, new LinkedList<string>()); // The dictionary provides this method with an amortised time of O(1)
        }

        /// <summary>
        /// Add an edge between vertex u and vertex v 
        /// This methods assumes an undirected graph
        /// </summary>
        /// <param name="u"></param>
        /// <param name="v"></param>
        public void AddEdge(String u, String v)
        {
            //Add v to the adjacnceis of u
            LinkedList<String> uAdjancies = Adjacencies(u);
            uAdjancies.AddLast(v);

            if (isUndirected)
            {
                LinkedList<String> vAdjancies = Adjacencies(v);
                vAdjancies.AddLast(u);
            }
        }


        public LinkedList<String> Adjacencies(String v)
        {
            return g[v];
        }

        public LinkedList<String> this[String v]
        {
            get
            {
                return Adjacencies(v);
            }
        }


        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (isUndirected)
            {
                sb.AppendLine("diagram G { ");

                foreach (String v in g.Keys)
                {
                    LinkedList<String> adj = Adjacencies(v);
                    foreach (String u in adj)
                    {   //BUG! Edges are added twice.  ! v--u and u--v both exist. You only want this to happen once. 

                        // v -> u
                        sb.AppendLine(
                            String.Format("\"{0}\" -> \"{1}\"", v, u)
                            );

                    }
                }
            }
            else
            {

            }

            sb.AppendLine("}");
            return sb.ToString();
        }
        #region depthFirstSearch

        public GraphAdjancencyList DFS(String source)
        {
            GraphAdjancencyList output = new GraphAdjancencyList(false); // this is a directed graph
            output.AddVertex(source);
            _DFS(output, source);

            return output;
        }

        private void _DFS(GraphAdjancencyList output, String s)
        {
            foreach (string v in Adjacencies(s))
            {
                if (!output.Contains(v))
                {
                    output.AddVertex(v);
                    output.AddEdge(s, v);
                    _DFS(output, v);
                }
            }
        }
        #endregion

        #region breathFirstSearch

        public GraphAdjancencyList BFS(String source)
        {
            Queue<String> grey = new Queue<string>();
            // HashSet<string> visted = new HashSet<string>();

            grey.Enqueue(source); //start from here

            GraphAdjancencyList output = new GraphAdjancencyList(false); // this is a directed graph
            output.AddVertex(source);

            while (grey.Count > 0)
            { //while queue is not empty
                String v = grey.Dequeue();// v is the next  vertex to visit
                foreach (string u in Adjacencies(v))
                {
                    if (!output.Contains(u))
                    {
                        output.AddVertex(u);
                        output.AddEdge(v, u);
                        grey.Enqueue(u); // u is the adjacent to v ... so wee neeed to visit it soon
                    }
                }
            }

            return output;
        }

        #endregion

    }
}