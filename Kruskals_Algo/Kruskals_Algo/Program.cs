﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruskals_Algo
{
    class Program
    {
        static void Main(string[] args)
        {
            WeightedGraphAdj g = new WeightedGraphAdj();


            g.AddVertex("A");
            g.AddVertex("B");
            g.AddVertex("C");
            g.AddVertex("D");
            g.AddVertex("E");
            g.AddVertex("F");
            g.AddVertex("G");
            g.AddVertex("H");

            g.AddEdge("A", "B",1);
            g.AddEdge("A", "C",3);
            g.AddEdge("B", "D",4);
            g.AddEdge("C", "D",5);
            g.AddEdge("D", "E",1);
            g.AddEdge("B", "F",1);
            g.AddEdge("F", "G",3);
            g.AddEdge("F", "H",2);
            g.AddEdge("G", "H",6);

//            LinkedList<String> adjacenies = g["A"];

            //WeightedGraphAdj result = g.DFS("A");
            //Console.WriteLine(result + "\n");
            //result = null;
            //result = g.BFS("A");
            //Console.WriteLine(result);
            //Console.ReadKey();

        }
    }
}
