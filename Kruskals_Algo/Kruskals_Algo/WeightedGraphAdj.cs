﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kruskals_Algo
{
    public class WeightedGraphAdj
    {

        private bool isUndirected;

        public WeightedGraphAdj(bool IsUndirected = true)
        {
            this.isUndirected = IsUndirected;
        }

        //Dicitionary is a hashtable that uses generics in c#
        /// <summary>
        /// The Data for the grpah is stored here as an adjacency list 
        /// </summary>        
        private Dictionary<
                String, //keys represent vertices 
                LinkedList<
                    Tuple<string,int>
                    >// The value (for each key) is a linked list of adjacent vertices and weights !
            > g = new Dictionary<string, LinkedList<Tuple<string,int>>>();


        public bool Contains(String v)
        {
            return g.ContainsKey(v); // The dictionary provbides this metod with an average time of o(1)
        }

        public void AddVertex(String v)
        {
            g.Add(v, new LinkedList<Tuple<string,int>>()); // The dictionary provides this method with an amortised time of O(1)
        }

        /// <summary>
        /// Add an edge between vertex u and vertex v 
        /// This methods assumes an undirected graph
        /// </summary>
        /// <param name="u"></param>
        /// <param name="v"></param>
        public void AddEdge(String u, String v,int weight)
        {
            //Add v to the adjacnceis of u
            LinkedList<Tuple<string,int>> uAdjancies = Adjacencies(u);
            uAdjancies.AddLast(new Tuple<string, int>(v, weight));

            if (isUndirected)
            {
                LinkedList<Tuple<string,int>> vAdjancies = Adjacencies(v);
                vAdjancies.AddLast(new Tuple<string, int>(v, weight));
            }
        }


        public LinkedList<Tuple<string,int>> Adjacencies(String v)
        {
            return g[v];
        }

        public LinkedList<Tuple<string,int>> this[String v]
        {
            get
            {
                return Adjacencies(v);
            }
        }


        public WeightedGraphAdj Kruskals() {
            //Graph MST = new Graph
            WeightedGraphAdj MST = new WeightedGraphAdj();
            Dictionary<String, List<String>> vertexSets = new Dictionary<string, List<String>>();
            //MST.V = G.V

            foreach (String  V in this.g.Keys)
            {
                MST.AddVertex(V);
                //Make-Set(V)

                List<String> newSet = new List<string>();
                newSet.Add(V);
                vertexSets.Add(V, newSet);
            }

            HashSet<UndirectedEdge> addedEdges = new HashSet<UndirectedEdge>();
            List<UndirectedEdge> EPrime = new List<UndirectedEdge>();

            foreach (String V in g.Keys)
            {
                foreach (Tuple<String,int> edge  in g[V])
                {

                    UndirectedEdge newEdge = new UndirectedEdge(V, edge.Item1);

                    if (!addedEdges.Contains(newEdge))
                    {
                        EPrime.Add(newEdge);
                        addedEdges.Add(newEdge);
                    }
                };
            }

            throw NotImplementedException();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            if (isUndirected)

            {
                HashSet<UndirectedEdge> drawnEdges = new HashSet<UndirectedEdge>();
                sb.AppendLine("graph G { ");

                foreach (String v in g.Keys)
                {
                    LinkedList<Tuple<string,int>> adj = Adjacencies(v);
                    foreach (Tuple<String,int> u in adj)
                    {   //BUG! Edges are added twice.  ! v--u and u--v both exist. You only want this to happen once. 
                        UndirectedEdge newEdge = new UndirectedEdge(v, u.Item1);
                        if (drawnEdges.Contains(newEdge))
                        {
                            //do nothing cause we already drew the edge
                        }
                        else
                        {
                            // v -> u
                            sb.AppendLine(
                                String.Format("\"{0}\" -> \"{1}\"", v, u)
                                );

                        }
                    }
                }
            }
            else
            {

            }

            sb.AppendLine("}");
            return sb.ToString();
        }

    }
}