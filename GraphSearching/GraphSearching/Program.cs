﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphSearching
{
    class Program
    {
        static void Main(string[] args)
        {
            GraphAdjancencyList g = new GraphAdjancencyList();


            g.AddVertex("A");
            g.AddVertex("B");
            g.AddVertex("C");
            g.AddVertex("D");
            g.AddVertex("E");
            g.AddVertex("F");
            g.AddVertex("G");
            g.AddVertex("H");

            g.AddEdge("A", "B");
            g.AddEdge("A", "C");
            g.AddEdge("B", "D");
            g.AddEdge("C", "D");
            g.AddEdge("D", "E");
            g.AddEdge("B", "F");
            g.AddEdge("F", "G");
            g.AddEdge("F", "H");
            g.AddEdge("G", "H");

            //LinkedList<String> adjacenies = g["A"];
            
            GraphAdjancencyList result = g.DFS("A");
            Console.WriteLine(result+"\n");
            result = null;
            result = g.BFS("A");
            Console.WriteLine(result);
            Console.ReadKey();
        }
    }
}
