﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment_1
{
    public class Edge 
    {
        private int weight;
        private string v;
        private bool isSmallesWeight = false;
        public int Weight { get => weight; set => weight = value; }
        public string V { get => v; set => v = value; }
        public bool IsSmallesWeight { get => isSmallesWeight; set => isSmallesWeight = value; }

        public Edge(int weight, string v, bool isSmallestWeight) {
            this.weight = weight;
            this.v = v;
            this.isSmallesWeight = isSmallesWeight;
        }
    }
}
