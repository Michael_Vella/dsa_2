﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment_1
{
    public class Min_Heap_Dijskra
    {
        public MinHeap Dijskra_Alg(Graph graph, string source)
        {
            MinHeap subGraph = null;
            MinHeap distinaces = new MinHeap(graph.GraphActual.Count, source);

            int i;
            for (i = 0; i < graph.GraphActual.Count; i++)
            {

                if (!graph.GraphActual.ElementAt(i).Key.Equals(source)) // copy values of graph into distances, set all except source weight to maax
                {

                    distinaces.Insert(new Tuple<string, int, string>(graph.GraphActual.ElementAt(i).Key, int.MaxValue - (graph.GraphActual.Count - i), ""));
                }

            }

            for (int a = 0; a < graph.GraphActual.Count - 1; a++)
            {
                Tuple<string, int, string> minGraph = distinaces.RemoveElement();

                if (subGraph == null)
                {
                    subGraph = new MinHeap(graph.GraphActual.Count, source);
                }
                else
                {
                    subGraph.Insert(minGraph);
                }
                LinkedList<Edge> graphAdj = graph.Adjacencies(minGraph.Item1);

                int s = 0;
                while (s < graph.GraphActual.Count)
                {
                    Tuple<string, int, string> returnedEdge = distinaces.GetVertex(graph.GraphActual.ElementAt(s).Key);
                    foreach (var adj in graphAdj)
                    {
                        if (returnedEdge != null && graph.GraphActual.ElementAt(s).Key.Equals(adj.V) && minGraph.Item2 + adj.Weight < returnedEdge.Item2)
                        {
                            distinaces.UpdateVertex(graph.GraphActual.ElementAt(s).Key, minGraph.Item2 + adj.Weight, minGraph.Item1);
                        }
                    }
                    s++;
                }
            }
            subGraph.Insert(distinaces.RemoveElement());
            return subGraph;
        }
    }
}
