﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment_1
{
    /// <summary>
    /// This class is based on this site's page: https://www.geeksforgeeks.org/min-heap-in-java/
    /// </summary>
    public class MinHeap
    {
        private Tuple<string, int, string>[] Heap;
        private int size;
        private int maxsize;
        private string source;

        private static readonly int FRONT = 0;

        public Tuple<string, int, string>[] Heap1 { get => Heap; set => Heap = value; }

        public MinHeap(int maxsize, string source)
        {
            this.source = source;
            this.maxsize = maxsize;
            this.size = 0;
            Heap = new Tuple<string, int, string>[maxsize + 1];
            Heap[0] = new Tuple<string, int, string>(source, 0, source);
        }


        // Function to return the position of  
        // the Parent for the node currently  
        // at pos 
        private int Parent(int pos)
        {
            return pos / 2;
        }

        // Function to return the position of the  
        // left child for the node currently at pos 
        private int LeftChild(int pos)
        {
            return (2 * pos);
        }

        // Function to return the position of  
        // the right child for the node currently  
        // at pos 
        private int RightChild(int pos)
        {
            return (2 * pos) + 1;
        }

        // Function that returns true if the passed  
        // node is a leaf node 
        private bool IsLeaf(int pos)
        {
            if (pos >= (size / 2) && pos <= size)
            {
                return true;
            }
            return false;
        }

        // Function to Swap two nodes of the heap 
        private void Swap(int fpos, int spos)
        {
            Tuple<string, int, string> tmp;
            tmp = Heap[fpos];
            Heap[fpos] = Heap[spos];
            Heap[spos] = tmp;
        }

        // Function to heapify the node at pos 
        private void MinHeapify(int pos)
        {

            // If the node is a non-leaf node and greater 
            // than any of its child 
            if (!IsLeaf(pos))
            {
                if (Heap[pos].Item2 > Heap[LeftChild(pos)].Item2 || Heap[pos].Item2 > Heap[RightChild(pos)].Item2)
                {

                    // Swap with the left child and heapify 
                    // the left child 
                    if (Heap[LeftChild(pos)].Item2 < Heap[RightChild(pos)].Item2)
                    {
                        Swap(pos, LeftChild(pos));
                        MinHeapify(LeftChild(pos));
                    }

                    // Swap with the right child and heapify  
                    // the right child 
                    else
                    {
                        Swap(pos, RightChild(pos));
                        MinHeapify(RightChild(pos));
                    }
                }
            }
        }

        // Function to insert a node into the heap 
        public void Insert(Tuple<string, int, string> element)
        {
            Heap[++size] = element;
            int current = size;

            while (Heap[current].Item2 < Heap[Parent(current)].Item2)
            {
                Swap(current, Parent(current));
                current = Parent(current);
            }
        }

        // Function to print the contents of the heap 
        public void Print()
        {
            for (int i = 1; i <= size / 2; i++)
            {
                Console.Write(" Parent : " + Heap[i]
                          + " LEFT CHILD : " + Heap[2 * i]
                        + " RIGHT CHILD :" + Heap[2 * i + 1] + "\n");

            }
        }

        // Function to build the min heap using  
        // the MinHeapify 
        public void MinHeapBuild()
        {
            for (int pos = (size / 2); pos >= 1; pos--)
            {
                MinHeapify(pos);
            }
        }

        // Function to remove and return the minimum 
        // element from the heap 
        public Tuple<string, int, string> RemoveElement()
        {
            MinHeapify(size);
            Tuple<string, int, string> popped = Heap[FRONT];
            Heap[FRONT] = Heap[size--];
            MinHeapify(FRONT);
            return popped;
        }

        //update current vertex in heap
        public void UpdateVertex(string childVertex, int weight, string parentVertex)
        {
            for (int i = 0; i < Heap.Length; i++)
            {
                if (Heap[i] != null && Heap[i].Item1.Equals(childVertex))
                {
                    Heap[i] = new Tuple<string, int, string>(childVertex, weight, parentVertex);
                }
            }

            MinHeapify(0);
        }


        // Get Vertex 
        public Tuple<String, int, String> GetVertex(String vertex)
        {
            for (int i = 0; i < Heap.Length; i++) //iterate through pre-existing heap 
            {
                if (Heap[i] != null && Heap[i].Item1.Equals(vertex))
                {
                    return Heap[i];
                }
            }
            return null;
        }


    }
}
