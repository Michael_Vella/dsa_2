﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment_1
{
    public  class Graph 
    {
        /*
         *  This object will be what represents the graph as a whole,
         *  The first string is a vertex, which will point to a value within 
         *  a linked list that contains the path to a another vertex and the 
         *  its weight value.
         * */

        private Dictionary<string, LinkedList<Edge>> graphActual = new Dictionary<string,LinkedList<Edge>>();
        
        public Dictionary<string, LinkedList<Edge>> GraphActual { get => graphActual; set => graphActual = value; }

        /// <summary>
        /// Adds a vertex
        /// </summary>
        /// <param name="v"> the vertex</param>
        public void addVertex(string v) {
            if (!graphActual.ContainsKey(v))
            {
                graphActual.Add(v, new LinkedList<Edge>());
            }
        }

        /// <summary>
        /// Returns true if the source is found within the graph.
        /// </summary>
        /// <param name="graph"></param>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool IsFound(Dictionary<string, LinkedList<Edge>> graph, string source) {
            return graph.ContainsKey(source);
        }


        public LinkedList<Edge> Adjacencies(String v)
        {
            return GraphActual[v]; // return the value associated with the key (Vertex) v.  This value is the adjacencies of v
        }

        public LinkedList<Edge> this[String v]
        {
            get
            {
                return Adjacencies(v);
            }
        }

        /// <summary>
        ///  Add an edge between vertex u and vertex v
        /// This method assumes an undirected graph!
        /// </summary>
        /// <param name="u"></param>
        /// <param name="v"></param>
        /// <param name="weight"></param>
        public void addEdgeVertex(string u, string v, int weight, bool isSmallestWeight)
        {
                // Add v to the adjacencies of u
                LinkedList<Edge> uAdjacencies = Adjacencies(u);
                uAdjacencies.AddLast(
                        new Edge(weight, v,isSmallestWeight)
                    );

                // an undirected graph will add BOTH edge u to v (above) AND edge v to u (below)
                LinkedList<Edge> vAdjacencies = Adjacencies(v);
                vAdjacencies.AddLast(
                        new Edge(weight,u,isSmallestWeight)
                    );
        }


    }
}
