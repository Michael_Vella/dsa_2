﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment_1
{
    class Dijskra_4
    {
        public Tuple<string, string, int, bool>[] Dijskra_Alg(Graph graph, string source)
        {
           
            Tuple<string, string, int, bool>[] distinaces = new Tuple<string, string, int, bool>[graph.GraphActual.Count];

            int i = 0;
            while (i <= graph.GraphActual.Count - 1)
            {
                
                if (graph.GraphActual.ElementAt(i).Key == source) // copy values of graph into distances, set all except source weight to maax
                {
                    
                    distinaces[i] = new Tuple<string, string, int, bool>(source, source, 0, false);
                }
                else
                {
                    distinaces[i] = new Tuple<string, string, int, bool>(graph.GraphActual.ElementAt(i).Key, "", int.MaxValue, false);
                }
                i++;
            }

            i = 0;
            while (i <= graph.GraphActual.Count - 1) //Iterate through all vertices present. 
            {
                int index = 0;
                int min = int.MaxValue;
                for (int a = 0; a < distinaces.Length; a++) //Extract minimum 
                {
                    if (distinaces[a].Item4 == false && distinaces[a].Item3 <= min) //Check if minimum and if weight is actually set as smallest value 
                    {
                        index = a;
                        min = distinaces[a].Item3;
                    }
                }
                distinaces[index] = new Tuple<string, string, int, bool>(distinaces[index].Item1, distinaces[index].Item2, distinaces[index].Item3, true);

                int v = 0;
                LinkedList<Edge> graphAdj = graph.Adjacencies(distinaces[index].Item1);
                while (v <= distinaces.Length - 1)
                {
                    foreach (var item in graphAdj) //Find and set the lowest weight according to the adjancies of the current current source.  
                    {
                        if (distinaces[v].Item4 == false && distinaces[v].Item1 == item.V && distinaces[index].Item3 + item.Weight <= distinaces[v].Item3)
                        {
                            distinaces[v] = new Tuple<string, string, int, bool>(distinaces[v].Item1, distinaces[index].Item1, distinaces[index].Item3 + item.Weight, distinaces[v].Item4);
                        }
                    }
                    v++;
                }

                i++;
            }

            return distinaces;
        }

        #region old_code
        //public Graph Initalise_Single_Source(Graph graph, string source)
        //{
        //    Graph subgraph = new Graph();
        //    if (graph.IsFound(graph.GraphActual, source))
        //    {
        //        subgraph.addVertex(source);
        //        subgraph.addEdgeVertex(source, source, 0, false);
        //    }

        //    return subgraph;
        //}

        //public Graph Dijskra(Graph graph, string source)
        //{

        //    Graph visited = Initalise_Single_Source(graph, source);

        //    int i = 0;
        //    int distance = int.MaxValue;
        //    int totalWeight = 0;
        //    string previous = source;
        //    int previousWeight = 0;
        //    List<string> visitedVertex = new List<string>();
        //    while (i != graph.GraphActual.Count)
        //    {
        //        LinkedList<Edge> graphAdj = graph.Adjacencies(visited.GraphActual.ElementAt(i).Key);
        //        List<Edge> sortedAdjGraph = graphAdj.ToList().OrderBy(x => x.Weight).ToList();


        //        foreach (var path in graphAdj)
        //        {
        //            if (path.V == previous)
        //            {
        //                sortedAdjGraph.Remove(path);
        //            }
        //        }

        //        foreach (var path in sortedAdjGraph)
        //        {

        //            if (!(path.V == previous))
        //            {
        //                totalWeight = path.Weight + previousWeight;
        //                if (totalWeight < distance)
        //                {
        //                    visited.addVertex(source);
        //                    visited.addVertex(path.V);
        //                    visited.addEdgeVertex(source, path.V, totalWeight, false);
        //                }
        //            }

        //            totalWeight = 0;
        //        }

        //        int maxWeight = int.MaxValue;

        //        LinkedList<Edge> visitedAdj = graph.Adjacencies(source);

        //        foreach (var newVertex in visitedAdj)
        //        {
        //            if (newVertex.V == source)
        //            {
        //                visitedAdj.Remove(newVertex);
        //            }
        //        }


        //        foreach (var newPath in visitedAdj)
        //        {

        //            if (newPath.Weight < maxWeight && !visitedVertex.Contains(newPath.V))
        //            {   //Setting new values for next iteration. 
        //                previous = source;
        //                source = newPath.V;
        //                maxWeight = newPath.Weight;
        //                previousWeight = maxWeight;
        //            }
        //        }

        //        //graph.GraphActual.Remove(graph.GraphActual.Keys.ElementAt(i));// remove the traveresed path 
        //        visitedVertex.Add(graph.GraphActual.Keys.ElementAt(i));
        //        i++;
        //    }

        //    return visited;

        //}
        #endregion
    }
}
