﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dsa_assignment_1
{
    class Program
    {
        static void Main(string[] args)
        {
            Graph graph = new Graph();

            graph.addVertex("A");
            graph.addVertex("F");
            graph.addVertex("E");
            graph.addVertex("J");
            graph.addVertex("I");
            graph.addVertex("D");
            graph.addVertex("C");
            graph.addVertex("H");
            graph.addVertex("B");
            graph.addVertex("G");
            graph.addEdgeVertex("A", "E", 2,false);
            graph.addEdgeVertex("A", "F", 1, false);
            graph.addEdgeVertex("E", "I", 3, false);
            graph.addEdgeVertex("F", "I", 2, false);
            graph.addEdgeVertex("F", "J", 3, false);
            graph.addEdgeVertex("I", "D", 1, false);
            graph.addEdgeVertex("J", "D", 2, false);
            graph.addEdgeVertex("A", "C", 4, false);
            graph.addEdgeVertex("C", "H", 2, false);
            graph.addEdgeVertex("H", "I", 4, false);
            graph.addEdgeVertex("C", "D", 6, false);
            graph.addEdgeVertex("D", "B", 4, false);
            graph.addEdgeVertex("B", "G", 1, false);

            Dijskra_4 dijskra = new Dijskra_4();

           // Graph visited = dijskra.Dijskra(graph, "A");

            // Console.WriteLine("\tPos\t||\tW\t||\tP Pos");
            //for (int i = 0; i < visited.GraphActual.Count; i++)
            //{
            //    Console.WriteLine(visited.GraphActual.ElementAt(i).Key, visited.GraphActual.ElementAt(i).Value.SelectMany(x => x.V).ToString());
            //}

            // Console.ReadKey();

            Tuple<string, string, int, bool>[] visited = dijskra.Dijskra_Alg(graph, "A");

            Console.WriteLine("\n\nSubgraph\n\n");
            foreach (var item in visited)
            {
                Console.WriteLine("Child Vertex: {0}\t Cummilative Weight: {1}\t Parent Vertex: {2}", item.Item1, item.Item3, item.Item2);

            }
            Console.ReadLine();

            Console.WriteLine("\n\nShortest path to J from source A \n\n");
            foreach (var item in visited)
            {
                if (item.Item1.Equals("A") || item.Item1.Equals("F") || item.Item1.Equals("J"))//Find the path to J, this is based on the above print.
                {
                    Console.WriteLine("Child Vertex: {0}\t Cummilative Weight: {1}\t Parent Vertex: {2}", item.Item1, item.Item3, item.Item2);
                }
            }
            Console.ReadLine();


            Min_Heap_Dijskra min_Heap_Dijskra = new Min_Heap_Dijskra();

            MinHeap minHeap = min_Heap_Dijskra.Dijskra_Alg(graph,"A");

            for (int i = 0; i < minHeap.Heap1.Length -1 ; i++)
            {
                Console.WriteLine("Child Vertex: {0}\t Cummilative Weight: {1}\t Parent Vertex: {2}", minHeap.Heap1[i].Item1, minHeap.Heap1[i].Item2, minHeap.Heap1[i].Item3);
            }

            Console.ReadLine();

        }
    }
}
